using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static 坦克大战_对象池控制器;

public class 坦克大战_敌方坦克 : MonoBehaviour
{
    public int tank_HP = 1000 ;

    public GameObject bomb;

    private void Awake()
    {
        //bomb = GameObject.Find("主战坦克炮弹_爆炸特效");
        
    }

    private void Update()
    {
        //血量为0销毁.设置销毁延时1。
        if ( tank_HP <= 0 )
        {
            Destroy(gameObject, 1);
        }


    }



    /// <summary>
    /// 敌人被我方攻击体攻击到，延时销毁我方攻击体。并且根据碰撞物体类型扣除血量。
    /// 销毁攻击到的敌人，同时自身销毁。
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "我方物品")
        {
            if (collision.gameObject.name == "主战坦克炮弹(Clone)")
            {
                tank_values.enemy_tank_HP = tank_HP -= tank_values.cannon_value;
            }
            else if (collision.gameObject.name == "主战坦克子弹(Clone)")
            {
                tank_values.enemy_tank_HP = tank_HP -= tank_values.gun_value;
            }
            else if (collision.gameObject.name == "主战坦克导弹(Clone)")
            {
                tank_values.enemy_tank_HP = tank_HP = 0;
            }
        }
        

    }







}
