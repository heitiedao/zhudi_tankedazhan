using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class 坦克大战_导弹 : MonoBehaviour, IGetFromPool_self
{
    private Rigidbody rigidbody1;

    public GameObject tank;

    private 坦克大战_对象池控制器 pool_controller;

    private void Awake()
    {
        rigidbody1 = gameObject.GetComponent<Rigidbody>();




    }
    private void Start()
    {
        pool_controller = 坦克大战_对象池控制器.Instance;
    }



    private void Update()
    {




    }


    //执行接口方法
    void IGetFromPool_self.Give_GetFromPool_self()
    {
        //在物体重新显露时初始化速度为0。保证不被之前的速度干扰。
        rigidbody1.velocity = Vector3.zero;
        //添加一个力。ForceMode.Impulse意思就是力固定作用1s。
        //Vector3 aa = gameObject.transform.forward * 100;
        //rigidbody1.AddForce(aa, ForceMode.Impulse);
    }






    private void OnCollisionEnter(Collision collision)
    {
        gameObject.SetActive(false);
        pool_controller.GetFromPool("主战坦克导弹_大爆炸特效", gameObject.transform.position, gameObject.transform.rotation);
    }





}
