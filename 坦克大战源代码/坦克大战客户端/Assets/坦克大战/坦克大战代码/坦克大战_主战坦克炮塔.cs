using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static 坦克大战_对象池控制器;

public class 坦克大战_主战坦克炮塔 : MonoBehaviour
{

    //public GameObject tank_cannon;//发射炮弹的预制体。需要外部拖入。
    private GameObject tank_cannon_mufflertube;
    private GameObject tank_gun_mufflertube;
    private GameObject tank_missile_create;

    private GameObject camera_front;

    private Rigidbody rigidbody_tank;

    private AudioSource gun_audioSource;
    private AudioSource cannon_audioSource;
    private AudioSource missile_audioSource;

    //调用控制器
    public 坦克大战_对象池控制器 pool_controller;





    private void Awake()
    {
        tank_cannon_mufflertube = GameObject.Find("炮弹生成位置");
        tank_gun_mufflertube = GameObject.Find("枪架/枪转轴/子弹生成位置");
        tank_missile_create = GameObject.Find("导弹生成位置");
        

        gun_audioSource = GameObject.Find("枪架/枪转轴/枪音效").GetComponent<AudioSource>();
        cannon_audioSource = GameObject.Find("炮音效").GetComponent<AudioSource>();
        missile_audioSource = GameObject.Find("导弹音效").GetComponent<AudioSource>();

        camera_front = GameObject.Find("前视镜相机");

        rigidbody_tank = GameObject.Find("主战坦克").GetComponent<Rigidbody>();

        //初始化控制器。把对象池加载出来。
        
    }

    private void Start()
    {
        //把已经填入完整数值的对象池调用过来。
        pool_controller = 坦克大战_对象池控制器.Instance;
    }

    private void Update()
    {
        //P1Fire_cannon();

        

        //生成预制体。
        P1Fire_cannon();




    }



    /// <summary>
    /// 发射炮弹。
    /// 另外还有发射机枪。机枪随鼠标转向。
    /// </summary>
    private float cannon_loadingtime = 0 ;
    private float gun_loadingtime = 0;
    private float missile_loadingtime = 0;
    private bool gun_switch = false ;
    private void P1Fire_cannon()
    {
        //装填时间1s。小于等于0代表装弹完成；大于0代表还在装弹。装弹时间考虑设置为可以调整的。
        if (cannon_loadingtime > 0)
        {
            cannon_loadingtime -= Time.deltaTime;
        }

        if (gun_loadingtime > 0)
        {
            gun_loadingtime -= Time.deltaTime;
        }
        if (missile_loadingtime > 0)
        {
            missile_loadingtime -= Time.deltaTime;
        }



        if (Input.GetKeyDown(KeyCode.J) && cannon_loadingtime <= 0)
        {
            //生成炮弹
            pool_controller.GetFromPool("主战坦克炮弹", tank_cannon_mufflertube.transform.position, tank_cannon_mufflertube.transform.rotation);
            //后坐力
            Vector3 aa = -gameObject.transform.forward * 50;
            rigidbody_tank.AddForce(aa, ForceMode.Impulse);

            cannon_loadingtime = 1;

            cannon_audioSource.PlayOneShot(cannon_audioSource.clip);
        }



        if (Input.GetKeyDown(KeyCode.U) && cannon_loadingtime <= 0)
        {
            //生成大量炮弹
            for (int i = 0; i < 5; i++)
            {
                pool_controller.GetFromPool("主战坦克炮弹", tank_cannon_mufflertube.transform.position, tank_cannon_mufflertube.transform.rotation);
                cannon_audioSource.PlayOneShot(cannon_audioSource.clip);
            }
            Vector3 aa = -gameObject.transform.forward * 100;
            rigidbody_tank.AddForce(aa, ForceMode.Impulse);
            cannon_loadingtime = 1;
        }
        if (Input.GetKeyDown(KeyCode.I) && missile_loadingtime <= 0)
        {
            //生成导弹.暂时启用。后面要改。
            pool_controller.GetFromPool("主战坦克导弹", tank_missile_create.transform.position, tank_missile_create.transform.rotation);
            missile_loadingtime = 1;

            missile_audioSource.PlayOneShot(missile_audioSource.clip);

        }


        //机枪拥有开关。因为总是因为误触鼠标乱射。
        if (Input.GetKeyDown(KeyCode.M))
        {
            if ( gun_switch )
            {
                tank_values.gun_switch = gun_switch = false;
            }
            else
            {
                tank_values.gun_switch = gun_switch = true;
            }
        }


        if (Input.GetMouseButton(0) && gun_loadingtime <= 0 && gun_switch)
        {
            //生成子弹
            //有bug。如果枪口模型压入了履带模型之内，射出的子弹就会飞行不正常了。暂时修改为把枪换了个位置。实际上应该锁定旋转范围。我为了试验效果，不那么做。
            pool_controller.GetFromPool("主战坦克子弹", tank_gun_mufflertube.transform.position, tank_gun_mufflertube.transform.rotation);
            gun_loadingtime = 0.15f;

            gun_audioSource.PlayOneShot(gun_audioSource.clip);
            


        }
        



        //切换镜头。

        if (Input.GetKeyDown(KeyCode.P))
        {
            if (camera_front.transform.localPosition !=new Vector3(0,0,1.6f))
            {
                camera_front.transform.localPosition = new Vector3(0, 0, 1.6f);
                
            }
            else
            {
                camera_front.transform.localPosition = new Vector3(0, 5, -5);
            }
        }


        






    }
}
