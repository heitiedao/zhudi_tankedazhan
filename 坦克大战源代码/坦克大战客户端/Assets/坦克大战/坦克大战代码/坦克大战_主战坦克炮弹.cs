using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;










public class 坦克大战_主战坦克炮弹 : MonoBehaviour, IGetFromPool_self
{
    private Rigidbody rigidbody1;

    public GameObject tank;
    private GameObject tank_turretturn;

    public 坦克大战_对象池控制器 pool_controller;

    private void Awake()
    {
        rigidbody1 = gameObject.GetComponent<Rigidbody>();

        //这条代码证明，Find可以寻找到任何物品。可以直接从最高级父物体开始找下去。
        tank_turretturn = GameObject.Find("主战坦克/炮塔转轴");



    }
    private void Start()
    {
        pool_controller = 坦克大战_对象池控制器.Instance;
    }



    private void Update()
    {
        



    }


    //执行接口方法
    void IGetFromPool_self.Give_GetFromPool_self()
    {
        //在物体重新显露时初始化速度为0。保证不被之前的速度干扰。
        rigidbody1.velocity = Vector3.zero;
        //添加一个力。ForceMode.Impulse意思就是力固定作用1s。
        Vector3 aa = gameObject.transform.forward * 10;
        rigidbody1.AddForce(aa, ForceMode.Impulse);
    }






    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "敌方")
        {
            //不可销毁炮弹、子弹。这是对象池循环使用的。
            gameObject.SetActive(false);
            pool_controller.GetFromPool("主战坦克炮弹_爆炸特效", gameObject.transform.position, gameObject.transform.rotation);
        }
        else if (collision.gameObject.tag == "建筑物")
        {
            pool_controller.GetFromPool("主战坦克炮弹_碰撞特效", gameObject.transform.position, gameObject.transform.rotation);
        }
        
    }





}
