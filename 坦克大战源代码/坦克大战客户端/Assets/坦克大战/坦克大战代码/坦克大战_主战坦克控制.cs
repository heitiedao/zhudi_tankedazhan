using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEngine.EventSystems.EventTrigger;


public static class tank_values
{
    public static int cannon_value = 500;
    public static int gun_value = 100;


    //缓存记录当前坦克数值的，仅此而已。
    public static int enemy_tank_HP;

    public static bool gun_switch = false;


    public static float speed_P1 ;
    public static float tank_turn_speed_P1 ;
    public static float tank_turretturn_speed_P1 ;


}







public class 坦克大战_主战坦克控制 : MonoBehaviour
{
    private Rigidbody rigidbody_P1;
    private GameObject tank_turretturn;
    private GameObject tank_gunturn;

    private Camera tank_camera;

    


    public float speed_P1 = 1;
    public float tank_turn_speed_P1 = 10;
    public float tank_turretturn_speed_P1 = 10;


    private void Awake()
    {
        rigidbody_P1 = GetComponent<Rigidbody>();

        tank_turretturn = GameObject.Find("炮塔转轴");
        tank_gunturn = GameObject.Find("炮塔转轴/枪架/枪转轴");

        tank_camera = GameObject.Find("炮塔转轴/前视镜相机").GetComponent<Camera>();


    }



    private void FixedUpdate()
    {
        P1Move_normal();

    }

    private void Update()
    {
        Func_Ray_Mouse();
        P1Move_turretturn();


        tank_values.speed_P1 = speed_P1;
        tank_values.tank_turn_speed_P1 = tank_turn_speed_P1;
        tank_values.tank_turretturn_speed_P1 = tank_turretturn_speed_P1;
    
    }









    /// <summary>
    /// 坦克整体移动。常规的移动方式，不适合坦克。
    /// </summary>
    private void P1Move_normal()
    {
        
        //获取输入
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");


        //旋转坦克
        rigidbody_P1.transform.Rotate(new Vector3(0, h, 0) * 0.02f * tank_turn_speed_P1, Space.World);


        //移动坦克
        rigidbody_P1.transform.Translate(new Vector3(0, 0, v) * 0.02f * speed_P1, Space.Self);



        //偏移量
        Vector3 tankbase_face = new Vector3(0, 0, v) * speed_P1 * 0.02f;


        rigidbody_P1.transform.Translate(tankbase_face, Space.Self);


        //小键盘调节坦克移速和转速
        if (Input.GetKeyDown(KeyCode.Keypad1) && speed_P1 > 0)
        {
            speed_P1 -= 1;
        }
        if (Input.GetKeyDown(KeyCode.Keypad2) && speed_P1 < 20)
        {
            speed_P1 += 1;
        }
        if (Input.GetKeyDown(KeyCode.Keypad4) && tank_turn_speed_P1 > 0)
        {
            tank_turn_speed_P1 -= 10;
        }
        if (Input.GetKeyDown(KeyCode.Keypad5) && tank_turn_speed_P1 < 50)
        {
            tank_turn_speed_P1 += 10;
        }









    }



    /// <summary>
    /// 坦克整体移动。坦克移动方法。左右转底盘调整方向，前后移动。
    /// </summary>
    private void P1Move_tank()
    {

        //获取输入
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        //偏移量
        Vector3 offset = new Vector3(h, 0, v) * speed_P1 * 0.02f;


        //rigidbody_P1.rotation();




    }




    /// <summary>
    /// 鼠标射线
    /// </summary> 
    Ray ray_mouse;
    RaycastHit thing_ray_mouse;
    private void Func_Ray_Mouse()
    {
        
        //射线开始于坦克相机，朝向鼠标方向。
        ray_mouse = tank_camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray_mouse, out thing_ray_mouse))
        {
            Debug.DrawLine(ray_mouse.origin, thing_ray_mouse.point, color: Color.red);
            tank_gunturn.transform.LookAt(thing_ray_mouse.point);
        }


    }




    /// <summary>
    /// 坦克炮塔转轴旋转
    /// 本来打算鼠标控制炮塔旋转。太复杂了。我阶段性放弃，搞出垃圾代码影响后续学习。就用键盘控制炮塔旋转。
    /// </summary>
    private void P1Move_turretturn()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            tank_turretturn.transform.Rotate(new Vector3(0, -1, 0) * 0.02f * tank_turretturn_speed_P1, Space.Self);
        }
        if ( Input.GetKey(KeyCode.E) )
        {
            tank_turretturn.transform.Rotate(new Vector3(0, 1, 0) * 0.02f * tank_turretturn_speed_P1, Space.Self);
        }



        //小键盘调节炮塔转速
        if (Input.GetKeyDown(KeyCode.Keypad7) && tank_turretturn_speed_P1 > 0)
        {
            tank_turretturn_speed_P1 -= 10;
        }
        if (Input.GetKeyDown(KeyCode.Keypad8) && tank_turretturn_speed_P1 < 50)
        {
            tank_turretturn_speed_P1 += 10;
        }







    }


    











}
